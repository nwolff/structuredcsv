package structuredcsv

import (
	"bytes"
	"encoding/csv"
	"io"
	"reflect"
	"strings"
	"testing"
)

const sampleCsv = `col1,col2
example@gmail.com,Ambre Claire
"a
continue",b
`

var sampleHeaders = newHeader([]string{"col1", "col2"})

var sampleFields = [][]string{
	{
		"example@gmail.com",
		"Ambre Claire",
	},
	{
		"a\ncontinue",
		"b",
	},
}

func TestRead(t *testing.T) {
	reader := NewReader(csv.NewReader(strings.NewReader(sampleCsv)))
	for _, expected := range sampleFields {
		sr, err := reader.Read()
		if err != nil {
			t.Fatalf("Unexpected error %v", err)
		}
		if !reflect.DeepEqual(sr.header, sampleHeaders) {
			t.Errorf("Expected %v, Got %v", sampleHeaders, sr.header)
		}
		if !reflect.DeepEqual(sr.fields, expected) {
			t.Errorf("Expected %v, Got %v", expected, sr.fields)
		}
	}
	_, err := reader.Read()
	if err != io.EOF {
		t.Errorf("Expected eof, got %v", err)
	}
}

func TestReadAllChannel(t *testing.T) {
	reader := NewReader(csv.NewReader(strings.NewReader(sampleCsv)))
	i := 0
	for record := range reader.ReadAllChannel() {
		expected := sampleFields[i]
		if !reflect.DeepEqual(record.fields, expected) {
			t.Errorf("Expected %v, Got %v", expected, record.fields)
		}
		i++
	}

	if reader.ReadAllError != nil {
		t.Fatal("Unexpected error", reader.ReadAllError)
	}
}

func TestGet(t *testing.T) {
	sr := Record{header: sampleHeaders, fields: sampleFields[0]}
	if sr.Get("col1") != "example@gmail.com" {
		t.Errorf("Expected %q, Got %q", "example@gmail.com", sr.Get("col1"))
	}
	if sr.Get("col2") != "Ambre Claire" {
		t.Errorf("Expected %q, Got %q", "Ambre Claire", sr.Get("col2"))
	}
	if sr.Get("col3") != "" {
		t.Errorf("Expected %q, Got %q", "", sr.Get("col3"))
	}
}

func TestWrite(t *testing.T) {
	var buf bytes.Buffer
	writer := NewWriter(csv.NewWriter(&buf))
	for _, fields := range sampleFields {
		writer.Write(&Record{sampleHeaders, fields})
	}
	writer.Flush()
	if buf.String() != sampleCsv {
		t.Errorf("Expected\n %q\n, Got\n %q", sampleCsv, buf.String())
	}
}
