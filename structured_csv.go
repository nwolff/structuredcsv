package structuredcsv

import (
	"encoding/csv"
	"fmt"
	"io"
)

type header struct {
	fieldNames []string
	indexes    map[string]int
	size       int
}

func newHeader(fieldNames []string) *header {
	h := header{fieldNames: fieldNames, size: len(fieldNames)}
	h.indexes = make(map[string]int)
	for i, fieldName := range fieldNames {
		h.indexes[fieldName] = i
	}
	return &h
}

// Record ...
type Record struct {
	header *header
	fields []string
}

// Get returns the value for the column with the given name, or "" if the column does not exist
func (sr *Record) Get(fieldName string) string {
	i, ok := sr.header.indexes[fieldName]
	if !ok {
		return ""
	}
	return sr.fields[i]
}

// We want to be able to wrap not only csv.Reader but also smaller, simpler readers
// This is just the interface we need to perform wrapping
type simpleCsvReader interface {
	Read() (record []string, err error)
}

// Reader wraps a simpleCsvReader
type Reader struct {
	r            simpleCsvReader
	header       *header
	ReadAllError error
}

// NewReader builds a Reader given a simpleCsvReader
func NewReader(r simpleCsvReader) *Reader {
	return &Reader{r: r}
}

// Read reads one StructuredRecord from r.
// The first line of the csv is taken as the headers
// Returns io.EOF at the end of iteration.
func (r *Reader) Read() (*Record, error) {
	record, err := r.r.Read()
	if err != nil {
		return nil, err
	}
	if r.header == nil {
		r.header = newHeader(record)
		return r.Read()
	}
	if r.header.size != len(record) {
		return nil, fmt.Errorf("Wrong number of items. Header: %v, Record: %v", r.header, record)
	}
	retval := Record{header: r.header, fields: record}
	return &retval, nil
}

// ReadAll reads all the records from r.
// A successful call returns err == nil, not err == EOF. Because ReadAll is
// defined to read until EOF, it does not treat end of file as an error to be
// reported.
func (r *Reader) ReadAll() (records []*Record, err error) {
	for {
		sr, err := r.Read()
		if err == io.EOF {
			return records, nil
		}
		if err != nil {
			return nil, err
		}
		records = append(records, sr)
	}
}

// ReadAllChannel reads all records from r into a channel
func (r *Reader) ReadAllChannel() chan *Record {
	c := make(chan *Record)
	go func() {
		defer close(c)
		for {
			sr, err := r.Read()
			if err == io.EOF {
				return
			}
			if err != nil {
				r.ReadAllError = err
				return
			}
			c <- sr
		}
	}()
	return c
}

// Writer wraps a csv.Writer
type Writer struct {
	w      *csv.Writer
	header *header
}

// NewWriter builds a Writer given a csv.Writer
func NewWriter(w *csv.Writer) *Writer {
	return &Writer{w: w}
}

// Writer writes a single structured record
func (w *Writer) Write(sr *Record) error {
	if w.header == nil {
		w.w.Write(sr.header.fieldNames)
		w.header = sr.header
	}
	if w.header != sr.header {
		return fmt.Errorf("Wrong header. Header: %v, StructuredRecord: %v", w.header, sr)
	}
	return w.w.Write(sr.fields)
}

// Flush writes any buffered data to the underlying io.Writer.
// To check if an error occurred during the Flush, call Error.
func (w *Writer) Flush() {
	w.w.Flush()
}

// Error reports any error that has occurred during a previous Write or Flush.
func (w *Writer) Error() error {
	return w.w.Error()
}
